<?php
$imgPng = imageCreateFromPng('img/banner.png');
imageAlphaBlending($imgPng, true);
imageSaveAlpha($imgPng, true);
header('Content-Type: image/png');
imagePng($imgPng);
$visitor_ip = $_SERVER['REMOTE_ADDR'];
$date = date("Y-m-d");
require_once( "classes/DataBaseManager.php" );
$dbm = new DataBaseManager;
if ($dbm->getVisits($date) == 0){
    $dbm->setFirstVisit($date);
	$dbm->addIP($visitor_ip);
}
else{
    if ($dbm->getIP($visitor_ip) === 1){
        $dbm->addHit($date);
    }
    else{
	    $dbm->addIP($visitor_ip);
	    $dbm->addHit($date);
	    $dbm->addHost($date);
    }
}