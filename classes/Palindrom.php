<?php
class Palindrom { 
    public function checkStr($str) { 
        $analyzedstr = $str;
        $str = mb_strtolower(str_replace(' ', '', $str), 'utf8'); 
        if (mb_strlen($str) < 2 ) {
            return "Строка \"$analyzedstr\" - не палиндром.<br/>";    
        }    
        elseif ($this->isPalindrom($str)) { 
            return "Строка \"$analyzedstr\" - палиндром.<br/>"; 
        }
        else{ 
            return "Строка \"$analyzedstr\" - не палиндром.<br/>"; 
        } 
    } 
    protected function isPalindrom($str) 
    { 
        return $str == $this->reverseStr($str); 
    } 
    protected function reverseStr($str) 
    { 
        preg_match_all('/./us', $str, $ar); 
        return join('', array_reverse($ar[0])); 
    } 
} 