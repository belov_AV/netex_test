<?php
class DataBaseManager {
	const db_host = "localhost";
    const db_name = "id5471935_statistic";
    const db_user = "id5471935_test";
    const db_pass = "qwerty";
 
    function __construct() {
        $this->mysqli = new mysqli(self::db_host, self::db_user, self::db_pass, self::db_name);
		if ($this->mysqli->connect_errno) {
            echo "Не удалось подключиться к MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
        }
    }

	public function getVisits($curDate) {
        $sql = "SELECT `visit_id` FROM `treatment` WHERE `date`= '$curDate'";
        return $this->mysqli->query($sql)->num_rows;
}
    
    public function setFirstVisit($curDate) {
        $this->clearIPtable();
        $sql = "INSERT INTO `treatment` SET `date`='$curDate', `hosts`=1,`views`=1";
        $this->mysqli->query($sql);
    }
	
	public function addIP($curIP) {
        $sql = "INSERT INTO `ip` SET `ip_address`= '$curIP'";
        $this->mysqli->query($sql);
    }
		
    public function getIP($curIP) {
        $sql = "SELECT `ip_id` FROM `ip` WHERE `ip_address`= '$curIP'";
        echo $this->mysqli->query($sql)->num_rows;
        return $this->mysqli->query($sql)->num_rows;
	}
		
    public function addHit($curDate) {
        $sql = "UPDATE `treatment` SET `views`=`views`+1 WHERE `date`= '$curDate'";
        $this->mysqli->query($sql);
    } 
	
    public function addHost($curDate) {
        $sql = "UPDATE `treatment` SET `hosts`=`hosts`+1 WHERE `date`= '$curDate'";
        $this->mysqli->query($sql);
    } 
     
	public function getStatistic($curDate) {
	    $sql = "SELECT `views`, `hosts` FROM `treatment` WHERE `date`= '$curDate'";
        return $this->mysqli->query($sql);
	}
	
	protected function clearIPtable() {
	    $sql = "DELETE FROM `ip`";
        $this->mysqli->query($sql);
	}	
	function __destruct() {
        $this->mysqli->close();
    }
}