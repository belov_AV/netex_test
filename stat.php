<!DOCTYPE HTML>
<html>
<head>
<meta charset = "utf-8" />
</head>
<body>
<?php include ("blocks/header.php") ?>
<h4>Статистика за <?php echo date("d.m.y"); ?>:</h4>
<?php require_once( "classes/DataBaseManager.php" );
$dbm = new DataBaseManager;
$row = $dbm->getStatistic(date("Y-m-d"))->fetch_assoc();
echo '<p>Количество уникальных просмотров страницы за день: ' . $row['hosts'] . '<br />';
echo 'Общее количество просмотров страницы за день: ' . $row['views'] . '</p>';
?>
</body>
</html>